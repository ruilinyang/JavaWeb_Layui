// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.SmsLogConstant;
import com.javaweb.system.entity.SmsLog;
import com.javaweb.system.mapper.SmsLogMapper;
import com.javaweb.system.query.SmsLogQuery;
import com.javaweb.system.service.ISmsLogService;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.system.vo.smslog.SmsLogListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 短信日志 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-05-04
 */
@Service
public class SmsLogServiceImpl extends BaseServiceImpl<SmsLogMapper, SmsLog> implements ISmsLogService {

    @Autowired
    private SmsLogMapper smsLogMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        SmsLogQuery smsLogQuery = (SmsLogQuery) query;
        // 查询条件
        QueryWrapper<SmsLog> queryWrapper = new QueryWrapper<>();
        // 手机号码
        if (!StringUtils.isEmpty(smsLogQuery.getMobile())) {
            queryWrapper.like("mobile", smsLogQuery.getMobile());
        }
        // 发送类型：1用户注册 2修改密码 3找回密码 4换绑手机号验证 5换绑手机号 6钱包提现 7设置支付密码 8系统通知
        if (smsLogQuery.getType() != null && smsLogQuery.getType() > 0) {
            queryWrapper.eq("type", smsLogQuery.getType());
        }
        // 状态：1成功 2失败 3待处理
        if (smsLogQuery.getStatus() != null && smsLogQuery.getStatus() > 0) {
            queryWrapper.eq("status", smsLogQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<SmsLog> page = new Page<>(smsLogQuery.getPage(), smsLogQuery.getLimit());
        IPage<SmsLog> data = smsLogMapper.selectPage(page, queryWrapper);
        List<SmsLog> smsLogList = data.getRecords();
        List<SmsLogListVo> smsLogListVoList = new ArrayList<>();
        if (!smsLogList.isEmpty()) {
            smsLogList.forEach(item -> {
                SmsLogListVo smsLogListVo = new SmsLogListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, smsLogListVo);
                // 发送类型描述
                if (smsLogListVo.getType() != null && smsLogListVo.getType() > 0) {
                    smsLogListVo.setTypeName(SmsLogConstant.SMSLOG_TYPE_LIST.get(smsLogListVo.getType()));
                }
                // 状态描述
                if (smsLogListVo.getStatus() != null && smsLogListVo.getStatus() > 0) {
                    smsLogListVo.setStatusName(SmsLogConstant.SMSLOG_STATUS_LIST.get(smsLogListVo.getStatus()));
                }
                // 添加人名称
                if (smsLogListVo.getCreateUser() > 0) {
                    smsLogListVo.setCreateUserName(UserUtils.getName((smsLogListVo.getCreateUser())));
                }
                // 更新人名称
                if (smsLogListVo.getUpdateUser() > 0) {
                    smsLogListVo.setUpdateUserName(UserUtils.getName((smsLogListVo.getUpdateUser())));
                }
                smsLogListVoList.add(smsLogListVo);
            });
        }
        return JsonResult.success("操作成功", smsLogListVoList, data.getTotal());
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        SmsLog entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

}