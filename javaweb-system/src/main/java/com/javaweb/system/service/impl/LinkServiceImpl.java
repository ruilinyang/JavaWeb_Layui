// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.LinkConstant;
import com.javaweb.system.entity.Item;
import com.javaweb.system.entity.Link;
import com.javaweb.system.mapper.ItemMapper;
import com.javaweb.system.mapper.LinkMapper;
import com.javaweb.system.query.LinkQuery;
import com.javaweb.system.service.IItemCateService;
import com.javaweb.system.service.ILinkService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.system.vo.link.LinkListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 友链 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-05-03
 */
@Service
public class LinkServiceImpl extends BaseServiceImpl<LinkMapper, Link> implements ILinkService {

    @Autowired
    private LinkMapper linkMapper;
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private IItemCateService itemCateService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        LinkQuery linkQuery = (LinkQuery) query;
        // 查询条件
        QueryWrapper<Link> queryWrapper = new QueryWrapper<>();
        // 友链名称
        if (!StringUtils.isEmpty(linkQuery.getName())) {
            queryWrapper.like("name", linkQuery.getName());
        }
        // 类型：1友情链接 2合作伙伴
        if (linkQuery.getType() != null && linkQuery.getType() > 0) {
            queryWrapper.eq("type", linkQuery.getType());
        }
        // 平台：1PC站 2WAP站 3微信小程序 4APP应用
        if (linkQuery.getPlatform() != null && linkQuery.getPlatform() > 0) {
            queryWrapper.eq("platform", linkQuery.getPlatform());
        }
        // 友链形式：1文字链接 2图片链接
        if (linkQuery.getForm() != null && linkQuery.getForm() > 0) {
            queryWrapper.eq("form", linkQuery.getForm());
        }
        // 状态：1在用 2停用
        if (linkQuery.getStatus() != null && linkQuery.getStatus() > 0) {
            queryWrapper.eq("status", linkQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<Link> page = new Page<>(linkQuery.getPage(), linkQuery.getLimit());
        IPage<Link> data = linkMapper.selectPage(page, queryWrapper);
        List<Link> linkList = data.getRecords();
        List<LinkListVo> linkListVoList = new ArrayList<>();
        if (!linkList.isEmpty()) {
            linkList.forEach(item -> {
                LinkListVo linkListVo = new LinkListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, linkListVo);
                // 类型描述
                if (linkListVo.getType() != null && linkListVo.getType() > 0) {
                    linkListVo.setTypeName(LinkConstant.LINK_TYPE_LIST.get(linkListVo.getType()));
                }
                // 平台描述
                if (linkListVo.getPlatform() != null && linkListVo.getPlatform() > 0) {
                    linkListVo.setPlatformName(LinkConstant.LINK_PLATFORM_LIST.get(linkListVo.getPlatform()));
                }
                // 友链形式描述
                if (linkListVo.getForm() != null && linkListVo.getForm() > 0) {
                    linkListVo.setFormName(LinkConstant.LINK_FORM_LIST.get(linkListVo.getForm()));
                }
                // 友链图片地址
                if (!StringUtils.isEmpty(linkListVo.getImage())) {
                    linkListVo.setImageUrl(CommonUtils.getImageURL(linkListVo.getImage()));
                }
                // 状态描述
                if (linkListVo.getStatus() != null && linkListVo.getStatus() > 0) {
                    linkListVo.setStatusName(LinkConstant.LINK_STATUS_LIST.get(linkListVo.getStatus()));
                }
                // 所属站点
                if (linkListVo.getItemId() != null && linkListVo.getItemId() > 0) {
                    Item itemInfo = itemMapper.selectById(linkListVo.getItemId());
                    if (itemInfo != null) {
                        linkListVo.setItemName(itemInfo.getName());
                    }
                }
                // 栏目
                String cateName = itemCateService.getCateNameByCateId(linkListVo.getCateId(), ">>");
                linkListVo.setCateName(cateName);
                // 添加人名称
                if (linkListVo.getCreateUser() > 0) {
                    linkListVo.setCreateUserName(UserUtils.getName((linkListVo.getCreateUser())));
                }
                // 更新人名称
                if (linkListVo.getUpdateUser() > 0) {
                    linkListVo.setUpdateUserName(UserUtils.getName((linkListVo.getUpdateUser())));
                }
                linkListVoList.add(linkListVo);
            });
        }
        return JsonResult.success("操作成功", linkListVoList, data.getTotal());
    }

    /**
     * 获取记录详情
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Link entity = (Link) super.getInfo(id);
        // 友链图片解析
        if (!StringUtils.isEmpty(entity.getImage())) {
            entity.setImage(CommonUtils.getImageURL(entity.getImage()));
        }
        return entity;
    }

    /**
     * 添加或编辑记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Link entity) {
        // 友链图片
        if (entity.getImage().contains(CommonConfig.imageURL)) {
            entity.setImage(entity.getImage().replaceAll(CommonConfig.imageURL, ""));
        }
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        Link entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(Link entity) {
        if (entity.getId() == null || entity.getId() <= 0) {
            return JsonResult.error("记录ID不能为空");
        }
        if (entity.getStatus() == null) {
            return JsonResult.error("记录状态不能为空");
        }
        return super.setStatus(entity);
    }
}