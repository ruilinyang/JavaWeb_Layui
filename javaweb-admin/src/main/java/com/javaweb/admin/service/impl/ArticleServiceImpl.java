// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.CommonUtils;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.admin.constant.ArticleConstant;
import com.javaweb.admin.entity.Article;
import com.javaweb.admin.mapper.ArticleMapper;
import com.javaweb.admin.query.ArticleQuery;
import com.javaweb.admin.service.IArticleService;
import com.javaweb.system.service.IItemCateService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.admin.vo.article.ArticleListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 文章管理 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-08-11
 */
@Service
public class ArticleServiceImpl extends BaseServiceImpl<ArticleMapper, Article> implements IArticleService {

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private IItemCateService itemCateService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        ArticleQuery articleQuery = (ArticleQuery) query;
        // 查询条件
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        // 文章标题
        if (!StringUtils.isEmpty(articleQuery.getTitle())) {
            queryWrapper.like("title", articleQuery.getTitle());
        }
        // 是否置顶：1已置顶 2未置顶
        if (articleQuery.getIsTop() != null && articleQuery.getIsTop() > 0) {
            queryWrapper.eq("is_top", articleQuery.getIsTop());
        }
        // 状态：1已审核 2待审核 3审核失败
        if (articleQuery.getStatus() != null && articleQuery.getStatus() > 0) {
            queryWrapper.eq("status", articleQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<Article> page = new Page<>(articleQuery.getPage(), articleQuery.getLimit());
        IPage<Article> data = articleMapper.selectPage(page, queryWrapper);
        List<Article> articleList = data.getRecords();
        List<ArticleListVo> articleListVoList = new ArrayList<>();
        if (!articleList.isEmpty()) {
            articleList.forEach(item -> {
                ArticleListVo articleListVo = new ArticleListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, articleListVo);
                // 首张图片编号地址
                if (!StringUtils.isEmpty(articleListVo.getCover())) {
                    articleListVo.setCoverUrl(CommonUtils.getImageURL(articleListVo.getCover()));
                }
                // 栏目分类
                if (item.getCateId() != null && item.getCateId() > 0) {
                    articleListVo.setCateName(itemCateService.getCateNameByCateId(item.getCateId(), ">>"));
                }
                // 是否置顶描述
                if (articleListVo.getIsTop() != null && articleListVo.getIsTop() > 0) {
                    articleListVo.setIsTopName(ArticleConstant.ARTICLE_ISTOP_LIST.get(articleListVo.getIsTop()));
                }
                // 状态描述
                if (articleListVo.getStatus() != null && articleListVo.getStatus() > 0) {
                    articleListVo.setStatusName(ArticleConstant.ARTICLE_STATUS_LIST.get(articleListVo.getStatus()));
                }
                // 添加人名称
                if (articleListVo.getCreateUser() != null && articleListVo.getCreateUser() > 0) {
                    articleListVo.setCreateUserName(UserUtils.getName((articleListVo.getCreateUser())));
                }
                // 更新人名称
                if (articleListVo.getUpdateUser() != null && articleListVo.getUpdateUser() > 0) {
                    articleListVo.setUpdateUserName(UserUtils.getName((articleListVo.getUpdateUser())));
                }
                articleListVoList.add(articleListVo);
            });
        }
        return JsonResult.success("操作成功", articleListVoList, data.getTotal());
    }

    /**
     * 获取记录详情
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Article entity = (Article) super.getInfo(id);
        // 首张图片编号解析
        if (!StringUtils.isEmpty(entity.getCover())) {
            entity.setCover(CommonUtils.getImageURL(entity.getCover()));
        }
        return entity;
    }

    /**
     * 添加或编辑记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Article entity) {
        // 首张图片编号
        if (entity.getCover().contains(CommonConfig.imageURL)) {
            entity.setCover(entity.getCover().replaceAll(CommonConfig.imageURL, ""));
        }
        // 图集处理
        String[] stringsVal = entity.getImgs().split(",");
        List<String> stringList = new ArrayList<>();
        for (String s : stringsVal) {
            if (s.contains(CommonConfig.imageURL)) {
                stringList.add(s.replaceAll(CommonConfig.imageURL, ""));
            } else {
                // 已上传图片
                stringList.add(s.replaceAll(CommonConfig.imageURL, ""));
            }
        }
        // 设置图集
        entity.setImgs(org.apache.commons.lang3.StringUtils.join(stringList, ","));
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        Article entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(Article entity) {
        if (entity.getId() == null || entity.getId() <= 0) {
            return JsonResult.error("记录ID不能为空");
        }
        if (entity.getStatus() == null) {
            return JsonResult.error("记录状态不能为空");
        }
        return super.setStatus(entity);
    }
}